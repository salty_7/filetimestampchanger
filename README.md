# FileTimeStampChanger #

ファイルのタイムスタンプを編集するWindwos向けプログラム。
https://bitbucket.org/salty_7/filetimestampchange/overview の.net版。

![SnapCrab_NoName_2017-1-29_1-5-33_No-00.png](https://bitbucket.org/repo/kopxLn/images/1827728391-SnapCrab_NoName_2017-1-29_1-5-33_No-00.png)

ダウンロード: https://bitbucket.org/salty_7/filetimestampchanger/downloads